﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class RoboManCharacterController : PhysicsObj
{
    public float jumpSpeed = 7;
    public float maxSpeed = 7;
    private int hp = 10;
    private SpriteRenderer spriteRenderer;
    private Animator animator;
    private Transform firePoint;
    private LayerMask powerups;
    public AudioSource bossMusic;
    void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        animator = GetComponent<Animator>();
        firePoint = transform.Find("FirePoint");
        
    }

    protected override void ComputeVelocity()
    {
       
        Vector2 move = Vector2.zero;
        move.x = Input.GetAxis("Horizontal");

        if (Input.GetKeyDown(KeyCode.UpArrow) && grounded){
            velocity.y = jumpSpeed;
         

        }
        else if (Input.GetKeyUp(KeyCode.UpArrow))
        {
            if (velocity.y > 0){
                velocity.y = velocity.y * 0.5f;
            }
        }
         
        bool flipSprite = (spriteRenderer.flipX ? (move.x > 0) : (move.x < 0));
        
        if (flipSprite)
        {
    
            spriteRenderer.flipX = !spriteRenderer.flipX;

            if (gameObject.GetComponent<SpriteRenderer>().flipX)
            {

             firePoint.localPosition = new Vector3(
             gameObject.transform.position.x * 0.0310f,
             firePoint.localPosition.y,
             firePoint.localPosition.z);

            }
            else
            {
                firePoint.localPosition = new Vector3(
                   gameObject.transform.position.x * -0.0310f,
                   firePoint.localPosition.y,
                   firePoint.localPosition.z);
            }
        }

        animator.SetBool("grounded", grounded);
        animator.SetFloat("velocityX", Mathf.Abs(velocity.x )/ maxSpeed);
        animator.SetFloat("time", Time.timeSinceLevelLoad+2f);
        targetVelocity = move * maxSpeed;
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.name == "Boss")
        {
            float magnitude = 0.5f;
            hp -= 5;
            var force = transform.position - collision.transform.position;
            animator.SetBool("hurt", true);
            if (hp == 0)
            {
                animator.SetBool("dead", true);
                gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(-0.1f* Time.deltaTime, 0);
            }
            else {
                Vector2 pushBack = new Vector2(force.x * magnitude, 0);
                gameObject.GetComponent<Rigidbody2D>().velocity = pushBack;
            }
        }
    }
    void gameOver()
    {
        gameObject.GetComponent<RoboManCharacterController>().enabled = false;
        SceneManager.LoadScene("main");
    }
    void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.collider.name == "Boss")
        {
            animator.SetBool("hurt", false);
        }
    }
  
    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.name == "BossTrigger")
        {
            GameObject boss = GameObject.Find("Boss");
            boss.GetComponent<Boss>().gravityModifier = 1f;
            boss.GetComponent<EnemyAI>().enabled = true;
            GameObject.Find("Main Camera").GetComponent<AudioSource>().enabled = false;
            gameObject.GetComponent<AudioSource>().enabled = true;
        }
        else
        {
            gameObject.GetComponent<ShootingScript>().powerup = true;
            collision.GetComponent<Renderer>().enabled = false;
        }
    }
}
