﻿using UnityEngine;
using Pathfinding;
using System.Collections;


public class EnemyAI : MonoBehaviour {
    public Transform target;
    public float updateRate = 2f;
    private Seeker seeker;
    private Rigidbody2D rb;

    //Calculated path
    public Path path;

    //Enemy speed (per second)
    public float speed = 300f;
    public ForceMode2D fMode;

    [HideInInspector]
    public bool pathEnded = false;

    //Max distance from Enemy to next way point
    public float nextWayPoint = 3f;
    private int currentWayPoint = 0;

    void Start()
    {
        seeker = GetComponent<Seeker>();
        rb = GetComponent<Rigidbody2D>();

        if (target == null)
        {
            Debug.LogError("No player selected");
            return;
        }

        seeker.StartPath(transform.position, target.position, OnPathComplete);

        StartCoroutine(UpdatePath());
    }

    IEnumerator UpdatePath()
    {
        if (target == null)
        {
            //TODO: insert a search function here
            yield break;
        }
        seeker.StartPath(transform.position, target.position, OnPathComplete);
        yield return new WaitForSeconds(1f / updateRate);
        StartCoroutine(UpdatePath());

    }

    public void OnPathComplete(Path p)
    {
        if (!p.error)
        {
            path = p;
            currentWayPoint = 0;
        }
    }

    void FixedUpdate()
    {
        if (target == null) {
            return;
        }

        if (rb.transform.position.x < target.position.x)
        {
            gameObject.GetComponent<SpriteRenderer>().flipX = true;
        }
        else
        {
            gameObject.GetComponent<SpriteRenderer>().flipX = false;
        }

        if (path == null)
        {
            return;
        }
        if (currentWayPoint >= path.vectorPath.Count)
        {
            if (pathEnded) {
                return;
            }
            else
            {
                pathEnded = true;
                return;
            }
        }
        pathEnded = false;

        //Direction to next way point 
        Vector3 direction = (path.vectorPath[currentWayPoint] - transform.position).normalized;
        direction *= speed * Time.fixedDeltaTime;

        //Move the enemy along the path
        rb.AddForce(direction, fMode);

        float distance = Vector3.Distance(transform.position, path.vectorPath[currentWayPoint]);
        if (distance < nextWayPoint)
        {
            currentWayPoint++;
            return;
        }
    }
}
