﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss : PhysicsObj
{
    private Animator animator;
    public float hp;
    // Use this for initialization
    void Start()
    {
        hp = 50;
        animator = GetComponent<Animator>();
        gravityModifier = 0f;
    }

    protected override void ComputeVelocity()
    {
        if (grounded)
        {   
            gameObject.GetComponent<EnemyAI>().enabled = true;
        }
    }
    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.name == "bullet(Clone)")
        {
            hp -= 10;
        }
    }
}
