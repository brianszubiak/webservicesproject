﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

    public float bulletSpeed;
    public RoboManCharacterController player;

    private void Start()
    {
        player = FindObjectOfType<RoboManCharacterController>();
        if (player.GetComponent<SpriteRenderer>().flipX)
        {
            bulletSpeed = -bulletSpeed;
            gameObject.GetComponent<SpriteRenderer>().flipX = true;
        }
    }
    void Update()
    {
        GetComponent<Rigidbody2D>().velocity = new Vector2(bulletSpeed, GetComponent<Rigidbody2D>().velocity.y);
        Destroy(gameObject, 2f);
    }

   void OnCollisionEnter2D(Collision2D collision)
    {
        GetComponent<Rigidbody2D>().velocity = new Vector2(0,0);
        Destroy(gameObject);
    }
}
