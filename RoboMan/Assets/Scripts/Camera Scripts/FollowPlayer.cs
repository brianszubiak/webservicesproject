﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour
{
    public Transform player;
    Vector3 velocity = Vector3.zero;
    public float smoothing = .15f;

    //Max Y Value for Camera
    public bool yMaxEnabled = false;
    public float yMaxValue = 0f;
    //Min Y Value for Camera
    public bool yMinEnabled = false;
    public float yMinValue = 0f;
    //Max X Value for Camera
    public bool xMaxEnabled = false;
    public float xMaxValue = 0f;
    //Min X Value for Camera
    public bool xMinEnabled = false;
    public float xMinValue = 0f;

    private void FixedUpdate()
    {
        //Get player position
        Vector3 playerPos = player.position;

        //Vertical
        if (yMinEnabled && yMaxEnabled)
        {
            playerPos.y = Mathf.Clamp(playerPos.y, yMinValue, yMaxValue);
        }
        else if (yMinEnabled)
        {
            playerPos.y = Mathf.Clamp(playerPos.y, yMinValue, playerPos.y);
        }
        else if (yMaxEnabled)
        {
            playerPos.y = Mathf.Clamp(playerPos.y, playerPos.y, yMaxValue);
        }
        //Horizontal
        if (xMinEnabled && xMaxEnabled)
        {
            playerPos.x = Mathf.Clamp(playerPos.x, xMinValue, xMaxValue);
        }
        else if (xMinEnabled)
        {
            playerPos.x = Mathf.Clamp(playerPos.x, xMinValue, playerPos.x);
        }
        else if (xMaxEnabled)
        {
            playerPos.x = Mathf.Clamp(playerPos.x, playerPos.x, xMaxValue);
        }


        //Align camera and the player "Z Position"
        playerPos.z = transform.position.z;

        transform.position = Vector3.SmoothDamp(transform.position, playerPos, ref velocity, smoothing);
    }
}
