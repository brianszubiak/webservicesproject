﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootingScript : MonoBehaviour
{

    public float fireRate = 0;
    public float Damage = 10;
    public LayerMask whatCanTakeDamage;
    public Transform firePoint;
    public GameObject bullet;
    public GameObject superBullet;
    public bool powerup;
    private float powerUpTimer=500f;

    // Use this for initialization
    void Awake()
    {
        firePoint = transform.Find("FirePoint");
        if (firePoint == null)
        {
            Debug.LogError("FirePoint Not Found");
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Shoot();
        }
        if (powerup)
        {
            powerUpTimer--;
        }

    }

    void Shoot()
    {
        if (powerup && powerUpTimer > 0f)
        {
            GameObject mBullet = Instantiate(superBullet, firePoint.position, firePoint.rotation);
            mBullet.GetComponent<Renderer>().sortingLayerName = "Player";
            bullet.GetComponent<Renderer>().enabled = false;
        }
        else
        { 
            bullet.GetComponent<Renderer>().enabled = true;
            GameObject mBullet = Instantiate(bullet, firePoint.position, firePoint.rotation);
            mBullet.GetComponent<Renderer>().sortingLayerName = "Player";
        }
    }
}
